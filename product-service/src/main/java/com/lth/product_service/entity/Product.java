package com.lth.product_service.entity;

import java.math.BigDecimal;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
public class Product {

  @Id
  private String id;
  private String name;
  private String description;
  private BigDecimal price;

  public static Product of(String name, String description, BigDecimal price) {
    Product product = new Product();
    product.setName(name);
    product.setDescription(description);
    product.setPrice(price);

    return product;
  }
}
