package com.lth.product_service.service.implement;

import com.lth.product_service.repository.ProductRepository;
import com.lth.product_service.service.ProductService;
import com.lth.product_service.dto.ProductDTO;
import com.lth.product_service.entity.Product;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductServiceImpl implements ProductService {

  private final ProductRepository productRepository;

  @Override
  public Product create(ProductDTO productDTO) {
    Product product = Product.of(
        productDTO.getName(),
        productDTO.getDescription(),
        productDTO.getPrice()
    );
    log.info("(create)product: {}", product);

    return productRepository.save(product);
  }

  @Override
  public List<Product> findAll() {
    return productRepository.findAll();
  }
}
