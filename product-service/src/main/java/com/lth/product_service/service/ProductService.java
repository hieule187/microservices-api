package com.lth.product_service.service;

import com.lth.product_service.dto.ProductDTO;
import com.lth.product_service.entity.Product;
import java.util.List;

public interface ProductService {

  Product create(ProductDTO productDTO);

  List<Product> findAll();
}
