package com.lth.product_service.dto;

import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ProductDTO {

  @NotNull
  private String name;
  @NotNull
  private String description;
  @NotNull
  private BigDecimal price;
}
