package com.lth.user_service.redis.model;

import com.lth.user_service.constant.RedisCacheConstant;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.TimeToLive;
import org.springframework.data.redis.core.index.Indexed;

@Data
@RedisHash(RedisCacheConstant.REDIS_TOKEN_CACHE)
public class TokenCache {

  @Id
  private String id;

  @Indexed
  private Long userId;

  private String token;

  private String type;

  // Time expiration in seconds
  @TimeToLive
  private Long expiration;

  public static TokenCache of(Long userId, String token, String type, Long expiration) {
    TokenCache tokenCache = new TokenCache();
    tokenCache.setUserId(userId);
    tokenCache.setToken(token);
    tokenCache.setType(type);
    tokenCache.setExpiration(expiration);

    return tokenCache;
  }

}
