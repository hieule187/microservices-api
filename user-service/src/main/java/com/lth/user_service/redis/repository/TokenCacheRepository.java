package com.lth.user_service.redis.repository;

import com.lth.user_service.redis.model.TokenCache;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenCacheRepository extends CrudRepository<TokenCache, String> {

  List<TokenCache> findByUserId(Long userId);

}
