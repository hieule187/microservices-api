package com.lth.user_service.redis.service.implement;

import com.lth.user_service.redis.model.TokenCache;
import com.lth.user_service.redis.repository.TokenCacheRepository;
import com.lth.user_service.redis.service.TokenCacheService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class TokenCacheServiceImpl implements TokenCacheService {

  private final TokenCacheRepository repository;

  @Override
  @Transactional
  public TokenCache create(TokenCache tokenCache) {
    log.info("(create)tokenCache.userId: {}", tokenCache.getUserId());
    return repository.save(tokenCache);
  }

  @Override
  public TokenCache findById(String id) {
    return repository.findById(id).orElse(null);
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    log.info("(deleteById)tokenCache.id: {}", id);
    repository.deleteById(id);
  }

  @Override
  @Transactional
  public void deleteByIds(List<String> ids) {
    log.info("(deleteByIds)tokenCache.ids: {}", ids);
    ids.forEach(repository::deleteById);
  }

  @Override
  public List<TokenCache> findByUserId(Long userId) {
    return repository.findByUserId(userId);
  }

  @Override
  @Transactional
  public void deleteAllByUserId(Long userId) {
    log.info("(deleteAllByUserId)tokenCache.userId: {}", userId);
    List<TokenCache> tokenCaches = repository.findByUserId(userId);
    if (!tokenCaches.isEmpty()) {
      repository.deleteAll(tokenCaches);
    }
  }

  @Override
  public List<TokenCache> findAll() {
    return (List<TokenCache>) repository.findAll();
  }

  @Override
  @Transactional
  public void deleteAll() {
    repository.deleteAll();
  }

}
