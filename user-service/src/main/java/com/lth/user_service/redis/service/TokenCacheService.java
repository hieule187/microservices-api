package com.lth.user_service.redis.service;

import com.lth.user_service.redis.model.TokenCache;
import java.util.List;

public interface TokenCacheService {

  TokenCache create(TokenCache tokenCache);

  TokenCache findById(String id);

  void deleteById(String id);

  void deleteByIds(List<String> ids);

  List<TokenCache> findByUserId(Long userId);

  void deleteAllByUserId(Long userId);

  List<TokenCache> findAll();

  void deleteAll();

}
