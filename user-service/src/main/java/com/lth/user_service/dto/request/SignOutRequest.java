package com.lth.user_service.dto.request;

import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class SignOutRequest {

  @NotBlank
  private String accessToken;

  @NotBlank
  private String refreshToken;

}
