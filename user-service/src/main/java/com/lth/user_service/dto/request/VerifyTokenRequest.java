package com.lth.user_service.dto.request;

import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class VerifyTokenRequest {

  @NotBlank
  private String accessToken;

}
