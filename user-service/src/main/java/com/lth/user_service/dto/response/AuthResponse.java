package com.lth.user_service.dto.response;

import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
public class AuthResponse {

  private String accessToken;

  private String refreshToken;

  private Long userId;

  private String username;

  private Set<String> roles;

}
