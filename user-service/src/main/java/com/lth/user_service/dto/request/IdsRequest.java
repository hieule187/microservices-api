package com.lth.user_service.dto.request;

import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class IdsRequest<I> {

  @NotNull
  private List<I> ids;

}
