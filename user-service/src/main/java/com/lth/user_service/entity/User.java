package com.lth.user_service.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lth.user_service.config.base.BaseEntity;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Data
@EntityListeners(AuditingEntityListener.class)
public class User extends BaseEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(unique = true)
  private String email;

  @Column(unique = true)
  private String phoneNumber;

  @Column(unique = true, nullable = false)
  private String username;

  @Column(nullable = false)
  @JsonIgnore
  private String password;

  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(
      name = "user_role",
      joinColumns = @JoinColumn(name = "user_id"),
      inverseJoinColumns = @JoinColumn(name = "role_id")
  )
  private Set<Role> roles = new HashSet<>();

  private Boolean enabled;

  private Boolean verified;

  public static User of(
      String email,
      String phoneNumber,
      String username,
      String passwordEncoded,
      Set<Role> roles
  ) {
    User user = new User();
    user.setEmail(email);
    user.setPhoneNumber(phoneNumber);
    user.setUsername(username);
    user.setPassword(passwordEncoded);
    user.setRoles(roles);
    user.setEnabled(Boolean.TRUE);
    user.setVerified(Boolean.FALSE);

    return user;
  }

}
