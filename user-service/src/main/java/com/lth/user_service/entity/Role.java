package com.lth.user_service.entity;

import com.lth.user_service.config.base.BaseEntity;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Data
@EntityListeners(AuditingEntityListener.class)
public class Role extends BaseEntity {

  @Id
  private String id;

  @Column(unique = true, nullable = false)
  private String name;

  private String description;

//  @ManyToMany(mappedBy = "roles", fetch = FetchType.EAGER)
//  private Set<User> users = new HashSet<>();

  public static Role of(String name, String description) {
    Role role = new Role();
    role.setName(name);
    role.setDescription(description);

    return role;
  }

  @PrePersist
  private void ensureId() {
    if (this.getId() == null || this.getId().isEmpty()) {
      this.setId(UUID.randomUUID().toString());
    }
  }

}
