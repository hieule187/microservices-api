package com.lth.user_service.repository.mysql;

import com.lth.user_service.config.base.BaseRepository;
import com.lth.user_service.entity.User;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends BaseRepository<User, Long> {

  Optional<User> findByUsername(String username);

  boolean existsByUsername(String username);

  boolean existsByEmail(String email);

  boolean existsByPhoneNumber(String phoneNumber);

  List<User> findByRoles_NameIn(Set<String> names);

}
