package com.lth.user_service.repository.mysql;

import com.lth.user_service.config.base.BaseRepository;
import com.lth.user_service.entity.Role;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends BaseRepository<Role, String> {

  Optional<Role> findByName(String name);

  List<Role> findByNameIn(Set<String> names);

}
