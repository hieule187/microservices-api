package com.lth.user_service.config.base;

import com.lth.user_service.exception.NotFoundException;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@Slf4j
public class BaseServiceImpl<T extends BaseEntity, I> implements BaseService<T, I> {

  private final BaseRepository<T, I> repository;

  public BaseServiceImpl(BaseRepository<T, I> repository) {
    this.repository = repository;
  }

  @Override
  public T findById(I id) {
    log.info("(findById)id: {}", id);
    return repository.findById(id).orElse(null);
  }

  @Override
  public T findByIdThrowNotFound(I id) {
    log.info("(findByIdThrowNotFound)id: {}", id);
    return repository.findById(id).orElseThrow(() -> new NotFoundException(String.valueOf(id)));
  }

  @Override
  public List<T> findByIds(List<I> ids) {
    log.info("(findByIds)ids: {}", ids);
    return repository.findAllById(ids);
  }

  @Override
  public List<T> findAll() {
    return repository.findAll();
  }

  @Override
  public List<T> findAllList(int page, int size) {
    return repository.findAll(PageRequest.of(page, size)).getContent();
  }

  @Override
  public Page<T> findAllPage(int page, int size) {
    return repository.findAll(PageRequest.of(page, size));
  }

  @Override
  public T create(T t) {
    log.info("(create)object: {}", t);
    return repository.save(t);
  }

  @Override
  public List<T> createAll(List<T> listOfT) {
    log.info("(createAll)objects: {}", listOfT);
    return repository.saveAll(listOfT);
  }

  @Override
  public T update(T t) {
    log.info("(update)object: {}", t);
    return repository.save(t);
  }

  @Override
  public List<T> updateAll(List<T> listOfT) {
    log.info("(updateAll)objects: {}", listOfT);
    return repository.saveAll(listOfT);
  }

  @Override
  public void deleteById(I id) {
    log.info("(deleteById)id: {}", id);
    repository.deleteById(id);
  }

  @Override
  public void deleteByIds(List<I> ids) {
    log.info("(deleteByIds)ids: {}", ids);
    repository.deleteAllById(ids);
  }

}
