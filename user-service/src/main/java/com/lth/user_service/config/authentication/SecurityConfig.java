package com.lth.user_service.config.authentication;

import com.lth.user_service.constant.CommonConstant;
import com.lth.user_service.constant.MessageSourceConstant;
import com.lth.user_service.exception.UnauthorizedException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.servlet.HandlerExceptionResolver;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
public class SecurityConfig {

  private static final String[] PUBLIC_ENDPOINTS = {
      "/api/v1/auth/**",
      "/api/v1/**/public/**"
  };

  private final CustomJwtDecoder customJwtDecoder;
  private final HandlerExceptionResolver resolver;

  public SecurityConfig(
      CustomJwtDecoder customJwtDecoder,
      @Qualifier("handlerExceptionResolver") HandlerExceptionResolver resolver
  ) {
    this.customJwtDecoder = customJwtDecoder;
    this.resolver = resolver;
  }

  @Bean
  public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
    httpSecurity.csrf()
        .disable()
        .authorizeHttpRequests()
        .antMatchers(PUBLIC_ENDPOINTS)
        .permitAll()
        .anyRequest()
        .authenticated();

    httpSecurity.oauth2ResourceServer(oauth2 -> oauth2
        .jwt(jwtConfigurer -> jwtConfigurer
            .decoder(customJwtDecoder)
            .jwtAuthenticationConverter(jwtAuthenticationConverter())
        )
        .authenticationEntryPoint(authenticationEntryPoint())
    );

    return httpSecurity.build();
  }

  @Bean
  JwtAuthenticationConverter jwtAuthenticationConverter() {
    JwtGrantedAuthoritiesConverter jwtGrantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
    jwtGrantedAuthoritiesConverter.setAuthorityPrefix(CommonConstant.ROLE_PREFIX);

    JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
    jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(jwtGrantedAuthoritiesConverter);

    return jwtAuthenticationConverter;
  }

  private AuthenticationEntryPoint authenticationEntryPoint() {
    return (request, response, exception) -> resolver.resolveException(
        request,
        response,
        null,
        new UnauthorizedException(MessageSourceConstant.UNAUTHORIZED_EXCEPTION)
    );
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

}
