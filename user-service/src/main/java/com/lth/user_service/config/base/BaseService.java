package com.lth.user_service.config.base;

import java.util.List;
import org.springframework.data.domain.Page;

public interface BaseService<T extends BaseEntity, I> {

  T findById(I id);

  T findByIdThrowNotFound(I id);

  List<T> findByIds(List<I> ids);

  List<T> findAll();

  List<T> findAllList(int page, int size);

  Page<T> findAllPage(int page, int size);

  T create(T t);

  List<T> createAll(List<T> listOfT);

  T update(T t);

  List<T> updateAll(List<T> listOfT);

  void deleteById(I id);

  void deleteByIds(List<I> ids);

}
