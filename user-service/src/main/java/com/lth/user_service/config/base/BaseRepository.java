package com.lth.user_service.config.base;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BaseRepository<T extends BaseEntity, I> extends JpaRepository<T, I> {

}
