package com.lth.user_service.config.audit;

import com.lth.user_service.constant.CommonConstant;
import java.util.Optional;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;

public class AuditorAwareImpl implements AuditorAware<Long> {

  @Override
  public Optional<Long> getCurrentAuditor() {
    Long currentUserId = CommonConstant.SYSTEM_ID;
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

    if (authentication != null && authentication.getPrincipal() != CommonConstant.ANONYMOUS_USER) {
      Jwt jwt = (Jwt) authentication.getPrincipal();

      if (jwt != null && jwt.getClaim("userId") != null) {
        currentUserId = Long.valueOf(jwt.getClaim("userId").toString());
      }
    }

    return Optional.of(currentUserId);
  }

}
