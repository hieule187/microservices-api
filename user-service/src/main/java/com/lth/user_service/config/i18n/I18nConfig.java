package com.lth.user_service.config.i18n;

import java.util.Locale;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

@Configuration
public class I18nConfig {

  @Bean
  ReloadableResourceBundleMessageSource messageSource() {
    ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
    messageSource.setBasenames("classpath:user-service/messages");
    messageSource.setDefaultEncoding("UTF-8");
    messageSource.setDefaultLocale(new Locale("en"));
    messageSource.setUseCodeAsDefaultMessage(true);

    return messageSource;
  }

}
