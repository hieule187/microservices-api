package com.lth.user_service.config.authentication;

import com.lth.user_service.entity.User;
import com.lth.user_service.redis.model.TokenCache;
import com.lth.user_service.redis.service.TokenCacheService;
import com.lth.user_service.service.TokenService;
import com.lth.user_service.service.UserService;
import com.nimbusds.jwt.SignedJWT;
import java.text.ParseException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtException;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class CustomJwtDecoder implements JwtDecoder {

  private final TokenCacheService tokenCacheService;

  @Override
  public Jwt decode(String token) throws JwtException {
    try {
      log.info("(decode)token: {}", token);

      TokenCache tokenCache = tokenCacheService.findById(token);
      if (tokenCache == null) {
        throw new JwtException("Invalid token 1");
      }

      log.info("(decode)jwt: {}", tokenCache.getToken());

      SignedJWT signedJWT = SignedJWT.parse(tokenCache.getToken());

      return new Jwt(tokenCache.getToken(),
          signedJWT.getJWTClaimsSet().getIssueTime().toInstant(),
          signedJWT.getJWTClaimsSet().getExpirationTime().toInstant(),
          signedJWT.getHeader().toJSONObject(),
          signedJWT.getJWTClaimsSet().getClaims()
      );

    } catch (ParseException e) {
      throw new JwtException("Invalid token 4");
    }
  }
}
