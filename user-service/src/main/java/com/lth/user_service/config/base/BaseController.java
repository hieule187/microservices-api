package com.lth.user_service.config.base;

import com.lth.user_service.dto.request.IdsRequest;
import java.util.List;
import javax.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

public class BaseController<T extends BaseEntity, I> {

  private final BaseService<T, I> service;

  public BaseController(BaseService<T, I> service) {
    this.service = service;
  }

  @GetMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  public T findById(@PathVariable @Valid I id) {
    return service.findByIdThrowNotFound(id);
  }

  @PostMapping("/ids")
  @ResponseStatus(HttpStatus.OK)
  public List<T> findByIds(@RequestBody @Valid IdsRequest<I> request) {
    return service.findByIds(request.getIds());
  }

  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  public List<T> findAllList(
      @RequestParam @Valid int page,
      @RequestParam @Valid int size
  ) {
    return service.findAllList(page, size);
  }

  @GetMapping("/pageable")
  @ResponseStatus(HttpStatus.OK)
  public Page<T> findAllPage(
      @RequestParam @Valid int page,
      @RequestParam @Valid int size
  ) {
    return service.findAllPage(page, size);
  }

}
