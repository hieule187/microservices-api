package com.lth.user_service.exception;

import com.lth.user_service.config.exception.CustomRuntimeException;
import com.lth.user_service.constant.MessageSourceConstant;
import javax.servlet.http.HttpServletResponse;

public class NotFoundException extends CustomRuntimeException {

  public NotFoundException(String id) {
    setStatus(HttpServletResponse.SC_NOT_FOUND);
    setError(MessageSourceConstant.NOT_FOUND_EXCEPTION);
    addParam("id", id);
  }

}
