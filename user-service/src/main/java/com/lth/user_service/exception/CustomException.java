package com.lth.user_service.exception;

import com.lth.user_service.config.exception.CustomRuntimeException;
import java.util.Map;

public class CustomException extends CustomRuntimeException {

  public CustomException(Integer status, String error) {
    setStatus(status);
    setError(error);
  }

  public CustomException(Integer status, String error, Map<String, String> params) {
    setStatus(status);
    setError(error);
    setParams(params);
  }

}
