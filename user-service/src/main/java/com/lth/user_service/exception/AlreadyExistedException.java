package com.lth.user_service.exception;

import com.lth.user_service.config.exception.CustomRuntimeException;
import com.lth.user_service.constant.MessageSourceConstant;
import javax.servlet.http.HttpServletResponse;

public class AlreadyExistedException extends CustomRuntimeException {

  public AlreadyExistedException(String id) {
    setStatus(HttpServletResponse.SC_BAD_REQUEST);
    setError(MessageSourceConstant.ALREADY_EXISTED_EXCEPTION);
    addParam("id", id);
  }

}
