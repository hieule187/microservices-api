package com.lth.user_service.exception;

import com.lth.user_service.config.exception.CustomRuntimeException;
import javax.servlet.http.HttpServletResponse;

public class UnauthorizedException extends CustomRuntimeException {

  public UnauthorizedException(String error) {
    setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    setError(error);
  }

}
