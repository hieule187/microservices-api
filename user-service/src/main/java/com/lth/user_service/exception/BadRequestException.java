package com.lth.user_service.exception;

import com.lth.user_service.config.exception.CustomRuntimeException;
import javax.servlet.http.HttpServletResponse;

public class BadRequestException extends CustomRuntimeException {

  public BadRequestException(String error) {
    setStatus(HttpServletResponse.SC_BAD_REQUEST);
    setError(error);
  }

}
