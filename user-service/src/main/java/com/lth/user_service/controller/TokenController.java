package com.lth.user_service.controller;

import com.lth.user_service.redis.model.TokenCache;
import com.lth.user_service.redis.service.TokenCacheService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/token")
@RequiredArgsConstructor
@Slf4j
public class TokenController {

  private final TokenCacheService tokenCacheService;

  @PostMapping("/public")
  @ResponseStatus(HttpStatus.CREATED)
  public TokenCache create(@RequestBody TokenCache tokenCache) {
    return tokenCacheService.create(tokenCache);
  }

  @DeleteMapping("/public/{id}")
  @ResponseStatus(HttpStatus.OK)
  public void deleteById(@PathVariable String id) {
    tokenCacheService.deleteById(id);
  }

  @GetMapping("/public")
  @ResponseStatus(HttpStatus.OK)
  public List<TokenCache> findAll() {
    return tokenCacheService.findAll();
  }

  @DeleteMapping("/public")
  @ResponseStatus(HttpStatus.OK)
  public void deleteAll() {
    tokenCacheService.deleteAll();
  }

  @GetMapping("/{id}")
  @PreAuthorize("hasAnyRole('ADMIN')")
  @ResponseStatus(HttpStatus.OK)
  public TokenCache findById(@PathVariable String id) {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    log.info("isAuthenticated: {}", authentication.isAuthenticated());
    log.info("getPrincipal: {}", authentication.getPrincipal().toString());
    log.info("getCredentials: {}", authentication.getCredentials().toString());
    log.info("getAuthorities: {}", authentication.getAuthorities());
    log.info("getName: {}", authentication.getName());

    Jwt jwt = (Jwt) authentication.getPrincipal();
    log.info("getClaims: {}", jwt.getClaims().get("userId"));

    return tokenCacheService.findById(id);
  }
}
