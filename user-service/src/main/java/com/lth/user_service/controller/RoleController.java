package com.lth.user_service.controller;

import com.lth.user_service.config.base.BaseController;
import com.lth.user_service.constant.RoleConstant;
import com.lth.user_service.entity.Role;
import com.lth.user_service.service.TokenService;
import com.lth.user_service.service.RoleService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/role")
public class RoleController extends BaseController<Role, String> {

  private final RoleService service;
  private final TokenService tokenService;

  public RoleController(RoleService service, TokenService tokenService) {
    super(service);
    this.service = service;
    this.tokenService = tokenService;
  }

  @PostMapping("/create-all")
  @RolesAllowed({RoleConstant.ADMIN})
  @ResponseStatus(HttpStatus.CREATED)
  public List<Role> createAll(@RequestBody @Valid List<Role> roles) {
    return service.createAll(roles);
  }

  @GetMapping("/token")
  @ResponseStatus(HttpStatus.OK)
  public Object getToken() {
//    String currentToken = tokenService.getCurrentToken();
    String adminToken = tokenService.getAdminToken();
    Map<String, String> response = new HashMap<>();
//    response.put("currentToken", currentToken);
    response.put("adminToken", adminToken);
    return response;
  }

}
