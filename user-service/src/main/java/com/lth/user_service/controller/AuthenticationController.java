package com.lth.user_service.controller;

import com.lth.user_service.constant.CommonConstant;
import com.lth.user_service.dto.request.RefreshTokenRequest;
import com.lth.user_service.dto.request.SignInRequest;
import com.lth.user_service.dto.request.SignOutRequest;
import com.lth.user_service.dto.request.SignUpRequest;
import com.lth.user_service.dto.request.VerifyTokenRequest;
import com.lth.user_service.dto.response.AuthResponse;
import com.lth.user_service.dto.response.SuccessResponse;
import com.lth.user_service.service.AuthenticationService;
import com.lth.user_service.service.TokenService;
import java.util.Objects;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/auth")
@RequiredArgsConstructor
public class AuthenticationController {

  private final AuthenticationService authenticationService;
  private final TokenService tokenService;

  @PostMapping("/sign-up-user")
  @ResponseStatus(HttpStatus.CREATED)
  public SuccessResponse<Objects> signUpUser(@RequestBody @Valid SignUpRequest signUpRequest) {
    authenticationService.signUpUser(signUpRequest);

    return SuccessResponse.of(
        HttpServletResponse.SC_CREATED,
        null
    );
  }

  @PostMapping("/sign-in")
  @ResponseStatus(HttpStatus.OK)
  public SuccessResponse<AuthResponse> signIn(@RequestBody @Valid SignInRequest signInRequest) {
    return SuccessResponse.of(
        HttpServletResponse.SC_OK,
        authenticationService.signIn(signInRequest)
    );
  }

  @PostMapping("/sign-out")
  @ResponseStatus(HttpStatus.OK)
  public SuccessResponse<Objects> signOut(@RequestBody @Valid SignOutRequest signOutRequest) {
    authenticationService.signOut(
        signOutRequest.getAccessToken(),
        signOutRequest.getRefreshToken()
    );

    return SuccessResponse.of(
        HttpServletResponse.SC_OK,
        null
    );
  }

  @PostMapping("/refresh-token")
  @ResponseStatus(HttpStatus.OK)
  public SuccessResponse<AuthResponse> refreshToken(
      @RequestBody @Valid RefreshTokenRequest refreshTokenRequest
  ) {
    return SuccessResponse.of(
        HttpServletResponse.SC_OK,
        authenticationService.refreshToken(refreshTokenRequest.getRefreshToken())
    );
  }

  @PostMapping("/verify-token")
  @ResponseStatus(HttpStatus.OK)
  public SuccessResponse<Boolean> verifyToken(
      @RequestBody @Valid VerifyTokenRequest verifyTokenRequest
  ) {
    return SuccessResponse.of(
        HttpServletResponse.SC_OK,
        Objects.nonNull(
            tokenService.verifyToken(
                verifyTokenRequest.getAccessToken(),
                CommonConstant.ACCESS_TYPE
            )
        )
    );
  }

}
