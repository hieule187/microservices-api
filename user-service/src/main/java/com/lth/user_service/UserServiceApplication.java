package com.lth.user_service;

import com.lth.user_service.entity.Role;
import com.lth.user_service.entity.User;
import com.lth.user_service.service.RoleService;
import com.lth.user_service.service.UserService;
import java.util.List;
import java.util.Set;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
@EnableEurekaClient
@EntityScan(basePackages = {"com.lth.user_service.entity"})
@EnableJpaRepositories(basePackages = {"com.lth.user_service.repository.mysql"})
public class UserServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(UserServiceApplication.class, args);
  }

  @Bean
  CommandLineRunner run(
      RoleService roleService,
      UserService userService,
      PasswordEncoder passwordEncoder
  ) {
    return args -> {
//      roleService.createAll(List.of(
//          Role.of("ADMIN", "Quản trị viên"),
//          Role.of("USER", "Người dùng")
//      ));
//      userService.create(User.of(
//          "admin@lth.com",
//          "0888888888",
//          "admin",
//          passwordEncoder.encode("123456aA@"),
//          Set.of(roleService.findByName("ADMIN")))
//      );
    };
  }

}
