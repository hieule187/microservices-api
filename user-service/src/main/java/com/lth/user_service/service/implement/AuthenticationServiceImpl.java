package com.lth.user_service.service.implement;

import com.lth.user_service.constant.CommonConstant;
import com.lth.user_service.constant.MessageSourceConstant;
import com.lth.user_service.constant.RegexConstant;
import com.lth.user_service.constant.RoleConstant;
import com.lth.user_service.dto.request.SignInRequest;
import com.lth.user_service.dto.request.SignUpRequest;
import com.lth.user_service.dto.response.AuthResponse;
import com.lth.user_service.entity.Role;
import com.lth.user_service.entity.User;
import com.lth.user_service.exception.AlreadyExistedException;
import com.lth.user_service.exception.BadRequestException;
import com.lth.user_service.exception.UnauthorizedException;
import com.lth.user_service.redis.model.TokenCache;
import com.lth.user_service.redis.service.TokenCacheService;
import com.lth.user_service.service.AuthenticationService;
import com.lth.user_service.service.RoleService;
import com.lth.user_service.service.TokenService;
import com.lth.user_service.service.UserService;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthenticationServiceImpl implements AuthenticationService {

  private final PasswordEncoder passwordEncoder;
  private final TokenService tokenService;
  private final UserService userService;
  private final RoleService roleService;
  private final TokenCacheService tokenCacheService;

  @Override
  @Transactional
  public void signUpUser(SignUpRequest signUpRequest) {
    log.info("(signUpUser)username: {}", signUpRequest.getUsername());

    validatorEmailFormat(signUpRequest.getEmail());
    validatorPhoneNumberFormat(signUpRequest.getPhoneNumber());
    validatorPasswordFormat(signUpRequest.getPassword());
    validatorExistsUserAccount(
        signUpRequest.getEmail(), signUpRequest.getPhoneNumber(), signUpRequest.getUsername()
    );

    Role roleUser = roleService.findByNameThrowNotFound(RoleConstant.USER);
    User user = User.of(
        signUpRequest.getEmail(),
        signUpRequest.getPhoneNumber(),
        signUpRequest.getUsername(),
        passwordEncoder.encode(signUpRequest.getPassword()),
        Set.of(roleUser)
    );

    userService.create(user);
  }

  @Override
  @Transactional
  public AuthResponse signIn(SignInRequest signInRequest) {
    log.info("(signIn)username: {}", signInRequest.getUsername());

    User user = userService.findByUsername(signInRequest.getUsername());
    if (user == null
        || !passwordEncoder.matches(signInRequest.getPassword(), user.getPassword())
        || Boolean.FALSE.equals(user.getEnabled())
    ) {
      throw new UnauthorizedException(
          MessageSourceConstant.USERNAME_OR_PASSWORD_INCORRECT_EXCEPTION
      );
    }

    return getAuthResponse(user);
  }

  @Override
  @Transactional
  public void signOut(String accessTokenId, String refreshTokenId) {
    log.info("(signOut)accessTokenId: {}, refreshTokenId: {}", accessTokenId, refreshTokenId);

    tokenCacheService.deleteByIds(List.of(accessTokenId, refreshTokenId));
  }

  @Override
  @Transactional
  public AuthResponse refreshToken(String refreshTokenId) {
    log.info("(refreshToken)refreshTokenId: {}", refreshTokenId);

    User user = tokenService.verifyToken(refreshTokenId, CommonConstant.REFRESH_TYPE);

    if (Objects.isNull(user)) {
      throw new UnauthorizedException(MessageSourceConstant.INVALID_TOKEN_EXCEPTION);
    }

    tokenCacheService.deleteById(refreshTokenId);

    return getAuthResponse(user);
  }

  private AuthResponse getAuthResponse(User user) {
    TokenCache accessToken = tokenService.generateToken(user, CommonConstant.ACCESS_TYPE);
    TokenCache refreshToken = tokenService.generateToken(user, CommonConstant.REFRESH_TYPE);

    Set<String> roles = user.getRoles().stream()
        .map(Role::getName)
        .collect(Collectors.toSet());

    return AuthResponse.of(
        accessToken.getId(),
        refreshToken.getId(),
        user.getId(),
        user.getUsername(),
        roles
    );
  }

  private void validatorEmailFormat(String email) {
    if (email == null) {
      return;
    }

    Matcher matcher = RegexConstant.EMAIL_PATTERN.matcher(email);
    if (!matcher.matches()) {
      throw new BadRequestException(MessageSourceConstant.INVALID_EMAIL_FORMAT_EXCEPTION);
    }
  }

  private void validatorPhoneNumberFormat(String phoneNumber) {
    if (phoneNumber == null) {
      return;
    }

    Matcher matcher = RegexConstant.PHONE_NUMBER_PATTERN.matcher(phoneNumber);
    if (!matcher.matches()) {
      throw new BadRequestException(MessageSourceConstant.INVALID_PHONE_NUMBER_FORMAT_EXCEPTION);
    }
  }

  private void validatorPasswordFormat(String password) {
    Matcher matcher = RegexConstant.PASSWORD_PATTERN.matcher(password);
    if (!matcher.matches()) {
      throw new BadRequestException(MessageSourceConstant.INVALID_PASSWORD_FORMAT_EXCEPTION);
    }
  }

  private void validatorExistsUserAccount(String email, String phoneNumber, String username) {
    if (email != null && userService.existsByEmail(email)) {
      throw new AlreadyExistedException(email);
    }
    if (phoneNumber != null && userService.existsByPhoneNumber(phoneNumber)) {
      throw new AlreadyExistedException(phoneNumber);
    }
    if (userService.existsByUsername(username)) {
      throw new AlreadyExistedException(username);
    }
  }

}
