package com.lth.user_service.service.implement;

import com.lth.user_service.constant.CommonConstant;
import com.lth.user_service.constant.MessageSourceConstant;
import com.lth.user_service.entity.User;
import com.lth.user_service.exception.CustomException;
import com.lth.user_service.exception.UnauthorizedException;
import com.lth.user_service.redis.model.TokenCache;
import com.lth.user_service.redis.service.TokenCacheService;
import com.lth.user_service.service.TokenService;
import com.lth.user_service.service.UserService;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.UUID;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class TokenServiceImpl implements TokenService {

  @Value("${application.security.jwt.secret-key}")
  private String secretKey;
  @Value("${application.security.jwt.access-token.expiration}")
  private long accessExpiration;
  @Value("${application.security.jwt.refresh-token.expiration}")
  private long refreshExpiration;

  private final UserService userService;
  private final TokenCacheService tokenCacheService;

  @Override
  public TokenCache generateToken(User user, String type) {
    String token = buildToken(
        user,
        Objects.equals(type, CommonConstant.ACCESS_TYPE) ? accessExpiration : refreshExpiration,
        type
    );

    return tokenCacheService.create(
        TokenCache.of(user.getId(), token, type, getRemainingTime(token))
    );
  }

  @Override
  public User verifyToken(String tokenId, String type) {
    TokenCache tokenCache = tokenCacheService.findById(tokenId);

    if (Objects.isNull(tokenCache)) {
      log.warn("(verifyToken)tokenCache is null");
      return null;
    }

    try {
      JWSVerifier verifier = new MACVerifier(secretKey.getBytes());

      SignedJWT signedJWT = SignedJWT.parse(tokenCache.getToken());

      boolean verified = signedJWT.verify(verifier);

      Date expiryTime = signedJWT.getJWTClaimsSet().getExpirationTime();

      boolean validType = Objects.equals(signedJWT.getJWTClaimsSet().getStringClaim("type"), type);

      String username = signedJWT.getJWTClaimsSet().getSubject();

      if (!verified || expiryTime.before(new Date()) || !validType || Objects.isNull(username)) {
        log.warn(
            "(verifyToken)verified: {}, expiryTime: {}, validType: {}, username: {}",
            verified, expiryTime, validType, username
        );
        return null;
      }

      User user = userService.findByUsername(username);

      if (Objects.isNull(user)) {
        log.warn("(verifyToken)username: {} does not exist", username);
        return null;
      }

      return user;
    } catch (JOSEException | ParseException exception) {
      throw new UnauthorizedException(MessageSourceConstant.INVALID_TOKEN_EXCEPTION);
    }
  }

  @Override
  public String getAdminToken() {
    User adminAccount = userService.findByUsername(CommonConstant.ADMIN_USERNAME);

    if (adminAccount == null) {
      return null;
    }

    List<TokenCache> tokenCaches = tokenCacheService.findByUserId(adminAccount.getId());

    if (!tokenCaches.isEmpty()) {
      return tokenCaches.get(0).getId();
    } else {
      TokenCache tokenCache = generateToken(adminAccount, CommonConstant.ACCESS_TYPE);
      return tokenCache.getId();
    }
  }

  private String buildToken(User user, long expiration, String type) {
    JWSHeader header = new JWSHeader(JWSAlgorithm.HS512);

    JWTClaimsSet jwtClaimsSet = new JWTClaimsSet.Builder()
        .subject(user.getUsername())
        .issuer("lth.vn")
        .issueTime(new Date())
        .expirationTime(new Date(System.currentTimeMillis() + expiration))
        .jwtID(UUID.randomUUID().toString())
        .claim("scope", buildScope(user))
        .claim("type", type)
        .claim("userId", user.getId())
        .build();

    Payload payload = new Payload(jwtClaimsSet.toJSONObject());

    JWSObject jwsObject = new JWSObject(header, payload);

    try {
      jwsObject.sign(new MACSigner(secretKey.getBytes()));
      return jwsObject.serialize();
    } catch (JOSEException exception) {
      throw new CustomException(
          HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
          MessageSourceConstant.INTERNAL_SERVER_ERROR
      );
    }
  }

  private String buildScope(User user) {
    StringJoiner stringJoiner = new StringJoiner(CommonConstant.LITERAL_SPACE);

    if (!user.getRoles().isEmpty()) {
      user.getRoles().forEach(role -> stringJoiner.add(role.getName()));
    }

    return stringJoiner.toString();
  }

  public Long getRemainingTime(String token) {
    try {
      SignedJWT signedJWT = SignedJWT.parse(token);

      Date expiryTime = signedJWT.getJWTClaimsSet().getExpirationTime();

      long currentTimeMillis = System.currentTimeMillis();

      long expiryTimeMillis = expiryTime.getTime();

      long remainingTime = (expiryTimeMillis - currentTimeMillis) / 1000;

      return remainingTime > 0 ? remainingTime : 0;
    } catch (ParseException exception) {
      throw new UnauthorizedException(MessageSourceConstant.INVALID_TOKEN_EXCEPTION);
    }
  }

}
