package com.lth.user_service.service;

import com.lth.user_service.dto.request.SignInRequest;
import com.lth.user_service.dto.request.SignUpRequest;
import com.lth.user_service.dto.response.AuthResponse;

public interface AuthenticationService {

  void signUpUser(SignUpRequest signUpRequest);

  AuthResponse signIn(SignInRequest signInRequest);

  void signOut(String accessTokenId, String refreshTokenId);

  AuthResponse refreshToken(String refreshTokenId);

}
