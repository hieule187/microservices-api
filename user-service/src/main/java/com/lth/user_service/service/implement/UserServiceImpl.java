package com.lth.user_service.service.implement;

import com.lth.user_service.config.base.BaseServiceImpl;
import com.lth.user_service.entity.User;
import com.lth.user_service.exception.NotFoundException;
import com.lth.user_service.repository.mysql.UserRepository;
import com.lth.user_service.service.UserService;
import java.util.List;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserServiceImpl extends BaseServiceImpl<User, Long> implements
    UserService {

  private final UserRepository repository;

  public UserServiceImpl(UserRepository repository) {
    super(repository);
    this.repository = repository;
  }

  @Override
  public User findByUsername(String username) {
    return repository.findByUsername(username).orElse(null);
  }

  @Override
  public User findByUsernameThrowNotFound(String username) {
    return repository.findByUsername(username).orElseThrow(() -> new NotFoundException(username));
  }

  @Override
  public boolean existsByUsername(String username) {
    return repository.existsByUsername(username);
  }

  @Override
  public boolean existsByEmail(String email) {
    return repository.existsByEmail(email);
  }

  @Override
  public boolean existsByPhoneNumber(String phoneNumber) {
    return repository.existsByPhoneNumber(phoneNumber);
  }

  @Override
  public List<User> findByRolesNameIn(Set<String> names) {
    return repository.findByRoles_NameIn(names);
  }

}
