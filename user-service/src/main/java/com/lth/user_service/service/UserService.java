package com.lth.user_service.service;

import com.lth.user_service.config.base.BaseService;
import com.lth.user_service.entity.User;
import java.util.List;
import java.util.Set;

public interface UserService extends BaseService<User, Long> {

  User findByUsername(String username);

  User findByUsernameThrowNotFound(String username);

  boolean existsByUsername(String username);

  boolean existsByEmail(String email);

  boolean existsByPhoneNumber(String phoneNumber);

  List<User> findByRolesNameIn(Set<String> names);

}
