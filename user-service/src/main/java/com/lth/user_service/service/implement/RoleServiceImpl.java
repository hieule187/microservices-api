package com.lth.user_service.service.implement;

import com.lth.user_service.config.base.BaseServiceImpl;
import com.lth.user_service.entity.Role;
import com.lth.user_service.exception.NotFoundException;
import com.lth.user_service.repository.mysql.RoleRepository;
import com.lth.user_service.service.RoleService;
import java.util.List;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RoleServiceImpl extends BaseServiceImpl<Role, String> implements
    RoleService {

  private final RoleRepository repository;

  public RoleServiceImpl(RoleRepository repository) {
    super(repository);
    this.repository = repository;
  }

  @Override
  public Role findByName(String name) {
    return repository.findByName(name).orElse(null);
  }

  @Override
  public Role findByNameThrowNotFound(String name) {
    return repository.findByName(name).orElseThrow(() -> new NotFoundException(name));
  }

  @Override
  public List<Role> findByNameIn(Set<String> names) {
    return repository.findByNameIn(names);
  }

}
