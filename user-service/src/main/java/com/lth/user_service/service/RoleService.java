package com.lth.user_service.service;

import com.lth.user_service.config.base.BaseService;
import com.lth.user_service.entity.Role;
import java.util.List;
import java.util.Set;

public interface RoleService extends BaseService<Role, String> {

  Role findByName(String name);

  Role findByNameThrowNotFound(String name);

  List<Role> findByNameIn(Set<String> names);

}
