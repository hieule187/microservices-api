package com.lth.user_service.service;

import com.lth.user_service.entity.User;
import com.lth.user_service.redis.model.TokenCache;

public interface TokenService {

  TokenCache generateToken(User user, String type);

  User verifyToken(String tokenId, String type);

  String getAdminToken();

}
