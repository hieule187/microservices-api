package com.lth.user_service.constant;

public class RedisCacheConstant {

  private RedisCacheConstant() {
  }

  public static final String REDIS_TOKEN_CACHE = "REDIS_TOKEN_CACHE";

}
