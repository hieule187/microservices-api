package com.lth.user_service.constant;

public class RoleConstant {

  private RoleConstant() {
  }

  public static final String ADMIN = "ADMIN";
  public static final String MANAGER = "MANAGER";
  public static final String USER = "USER";
  public static final String USER_PREMIUM = "USER_PREMIUM";

}
