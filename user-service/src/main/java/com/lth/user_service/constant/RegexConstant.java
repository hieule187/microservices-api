package com.lth.user_service.constant;

import java.util.regex.Pattern;

public class RegexConstant {

  private RegexConstant() {
  }

  public static final String EMAIL_REGEX =
      "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
  public static final String PHONE_NUMBER_REGEX = "^(0[3|5|7|8|9])+([0-9]{8})$";
  public static final String PASSWORD_REGEX =
      "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,20}$";

  public static final Pattern EMAIL_PATTERN = Pattern.compile(RegexConstant.EMAIL_REGEX);
  public static final Pattern PHONE_NUMBER_PATTERN =
      Pattern.compile(RegexConstant.PHONE_NUMBER_REGEX);
  public static final Pattern PASSWORD_PATTERN = Pattern.compile(RegexConstant.PASSWORD_REGEX);

}
