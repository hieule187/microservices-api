package com.lth.user_service.constant;

public class MessageSourceConstant {

  private MessageSourceConstant() {
  }

  public static final String INTERNAL_SERVER_ERROR = "com.lth.user_service.exception.InternalServerError";
  public static final String CONSTRAINT_VIOLATION_EXCEPTION = "com.lth.user_service.exception.ConstraintViolationException";
  public static final String MISSING_PARAMETER_EXCEPTION = "com.lth.user_service.exception.MissingParameterException";
  public static final String TYPE_MISMATCH_EXCEPTION = "com.lth.user_service.exception.TypeMismatchException";
  public static final String METHOD_ARGUMENT_NOT_VALID_EXCEPTION = "com.lth.user_service.exception.MethodArgumentNotValidException";
  public static final String HTTP_MESSAGE_NOT_READABLE_EXCEPTION = "com.lth.user_service.exception.HttpMessageNotReadableException";
  public static final String METHOD_NOT_SUPPORTED_EXCEPTION = "com.lth.user_service.exception.MethodNotSupportedException";
  public static final String NOT_FOUND_EXCEPTION = "com.lth.user_service.exception.NotFoundException";
  public static final String NO_HANDLER_FOUND_EXCEPTION = "com.lth.user_service.exception.NoHandlerFoundException";
  public static final String UNAUTHORIZED_EXCEPTION = "com.lth.user_service.exception.UnauthorizedException";
  public static final String ACCESS_DENIED_EXCEPTION = "com.lth.user_service.exception.AccessDeniedException";
  public static final String USERNAME_OR_PASSWORD_INCORRECT_EXCEPTION = "com.lth.user_service.exception.UsernameOrPasswordIncorrectException";
  public static final String INVALID_TOKEN_EXCEPTION = "com.lth.user_service.exception.InvalidTokenException";
  public static final String ALREADY_EXISTED_EXCEPTION = "com.lth.user_service.exception.AlreadyExistedException";
  public static final String INVALID_EMAIL_FORMAT_EXCEPTION = "com.lth.user_service.exception.InvalidEmailFormatException";
  public static final String INVALID_PHONE_NUMBER_FORMAT_EXCEPTION = "com.lth.user_service.exception.InvalidPhoneNumberFormatException";
  public static final String INVALID_PASSWORD_FORMAT_EXCEPTION = "com.lth.user_service.exception.InvalidPasswordFormatException";
  public static final String GET_DATA_SUCCESSFULLY = "com.lth.user_service.message.GetDataSuccessfully";
  public static final String CREATED_SUCCESSFULLY = "com.lth.user_service.message.CreatedSuccessfully";
  public static final String UPDATED_SUCCESSFULLY = "com.lth.user_service.message.UpdatedSuccessfully";
  public static final String SUCCESSFULLY = "com.lth.user_service.message.Successfully";

}
