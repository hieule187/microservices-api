package com.lth.user_service.constant;

public class CommonConstant {

  private CommonConstant() {
  }

  public static final String EMPTY_STRING = "";
  public static final String LITERAL_SPACE = " ";
  public static final String ROLE_PREFIX = "ROLE_";
  public static final String ACCESS_TYPE = "ACCESS";
  public static final String REFRESH_TYPE = "REFRESH";
  public static final Long SYSTEM_ID = 0L;
  public static final String ANONYMOUS_USER = "anonymousUser";
  public static final String ADMIN_USERNAME = "admin";

}
