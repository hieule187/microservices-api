package com.lth.order_service.controller;

import com.lth.order_service.service.ProductOrderService;
import com.lth.order_service.dto.ProductOrderDTO;
import com.lth.order_service.entity.ProductOrder;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/order")
@RequiredArgsConstructor
public class ProductOrderController {

  private final ProductOrderService productOrderService;

  @PostMapping()
  @ResponseStatus(HttpStatus.CREATED)
  public ProductOrder create(@RequestBody @Valid ProductOrderDTO productOrderDTO) {
    return productOrderService.create(productOrderDTO);
  }

  @GetMapping()
  @ResponseStatus(HttpStatus.OK)
  public List<ProductOrder> findAll() {
    return productOrderService.findAll();
  }
}
