package com.lth.order_service.entity;

import java.util.List;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import lombok.Data;

@Entity
@Data
public class ProductOrder {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column(unique = true)
  private String orderCode;
  @OneToMany(cascade = CascadeType.ALL)
  @JoinColumn(name = "order_id")
  private List<OrderLineItem> orderLineItems;

  @PrePersist
  private void ensureOrderCode() {
    if (this.getOrderCode() == null || this.getOrderCode().isEmpty()) {
      this.setOrderCode(UUID.randomUUID().toString());
    }
  }
}
