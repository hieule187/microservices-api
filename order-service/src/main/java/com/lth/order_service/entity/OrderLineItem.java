package com.lth.order_service.entity;

import java.math.BigDecimal;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import lombok.Data;

@Entity
@Data
public class OrderLineItem {

  @Id
  private String id;
  private String skuCode;
  private BigDecimal price;
  private Integer quantity;

  @PrePersist
  private void ensureId() {
    if (this.getId() == null || this.getId().isEmpty()) {
      this.setId(UUID.randomUUID().toString());
    }
  }
}
