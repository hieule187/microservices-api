package com.lth.order_service.service;

import com.lth.order_service.dto.ProductOrderDTO;
import com.lth.order_service.entity.ProductOrder;
import java.util.List;

public interface ProductOrderService {

  ProductOrder create(ProductOrderDTO productOrderDTO);

  List<ProductOrder> findAll();
}
