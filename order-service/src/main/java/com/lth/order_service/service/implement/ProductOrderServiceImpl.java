package com.lth.order_service.service.implement;

import com.lth.order_service.dto.InventoryDTO;
import com.lth.order_service.dto.ProductOrderDTO;
import com.lth.order_service.dto.SkuCodeDTO;
import com.lth.order_service.entity.OrderLineItem;
import com.lth.order_service.repository.ProductOrderRepository;
import com.lth.order_service.service.ProductOrderService;
import com.lth.order_service.dto.OrderLineItemDTO;
import com.lth.order_service.entity.ProductOrder;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductOrderServiceImpl implements ProductOrderService {

  private final ProductOrderRepository productOrderRepository;
  private final WebClient.Builder webClientBuilder;

  @Override
  public ProductOrder create(ProductOrderDTO productOrderDTO) {
    ProductOrder productOrder = new ProductOrder();
    List<OrderLineItem> orderLineItems = productOrderDTO.getOrderLineItems()
        .stream()
        .map(this::mapToEntity)
        .collect(Collectors.toList());
    productOrder.setOrderLineItems(orderLineItems);

    List<String> skuCodes = orderLineItems
        .stream()
        .map(OrderLineItem::getSkuCode)
        .collect(Collectors.toList());

    SkuCodeDTO skuCodeDTO = new SkuCodeDTO();
    skuCodeDTO.setSkuCodes(skuCodes);

    List<InventoryDTO> inventoryData = webClientBuilder.build().post()
        .uri("http://inventory-service/api/v1/inventory/find-by-sku-codes")
        .bodyValue(skuCodeDTO)
        .retrieve()
        .bodyToFlux(InventoryDTO.class)
        .collectList()
        .block();
    log.info("(create)inventoryData: {}", inventoryData);

    boolean allProductInStock = false;
    if (inventoryData != null && !inventoryData.isEmpty()) {
      allProductInStock = inventoryData
          .stream()
          .allMatch(inventory -> inventory.getQuantity() > 0);
      log.info("(create)allProductInStock: {}", allProductInStock);
      log.info("(create)productOrder: {}", productOrder);
    }

    if (Boolean.TRUE.equals(allProductInStock)) {
      return productOrderRepository.save(productOrder);
    } else {
      return null;
    }
  }

  @Override
  public List<ProductOrder> findAll() {
    return productOrderRepository.findAll();
  }

  private OrderLineItem mapToEntity(OrderLineItemDTO orderLineItemDTO) {
    OrderLineItem orderLineItem = new OrderLineItem();
    orderLineItem.setSkuCode(orderLineItemDTO.getSkuCode());
    orderLineItem.setPrice(orderLineItemDTO.getPrice());
    orderLineItem.setQuantity(orderLineItemDTO.getQuantity());

    return orderLineItem;
  }
}
