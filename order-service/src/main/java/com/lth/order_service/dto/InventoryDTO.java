package com.lth.order_service.dto;

import lombok.Data;

@Data
public class InventoryDTO {

  private Long id;
  private String skuCode;
  private Integer quantity;
}
