package com.lth.order_service.dto;

import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class OrderLineItemDTO {

  @NotNull
  private String skuCode;
  @NotNull
  private BigDecimal price;
  @NotNull
  private Integer quantity;
}
