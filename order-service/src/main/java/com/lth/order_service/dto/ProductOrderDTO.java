package com.lth.order_service.dto;

import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ProductOrderDTO {

  @NotNull
  private List<OrderLineItemDTO> orderLineItems;
}
