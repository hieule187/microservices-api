package com.lth.inventory_service.dto;

import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class InventoryDTO {

  @NotNull
  private String skuCode;
  @NotNull
  private Integer quantity;
}
