package com.lth.inventory_service.repository;

import com.lth.inventory_service.entity.Inventory;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InventoryRepository extends JpaRepository<Inventory, Long> {

  Inventory findBySkuCode(String skuCode);

  List<Inventory> findBySkuCodeIn(List<String> skuCodes);
}
