package com.lth.inventory_service.controller;

import com.lth.inventory_service.dto.SkuCodeDTO;
import com.lth.inventory_service.service.InventoryService;
import com.lth.inventory_service.dto.InventoryDTO;
import com.lth.inventory_service.entity.Inventory;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/inventory")
@RequiredArgsConstructor
public class InventoryController {

  private final InventoryService inventoryService;

  @PostMapping()
  @ResponseStatus(HttpStatus.CREATED)
  public Inventory create(@RequestBody @Valid InventoryDTO inventoryDTO) {
    return inventoryService.create(inventoryDTO);
  }

  @GetMapping()
  @ResponseStatus(HttpStatus.OK)
  public List<Inventory> findAll() {
    return inventoryService.findAll();
  }

  @GetMapping("/{skuCode}")
  @ResponseStatus(HttpStatus.OK)
  public Inventory findBySkuCode(@PathVariable @Valid String skuCode) {
    return inventoryService.findBySkuCode(skuCode);
  }

  @PostMapping("/find-by-sku-codes")
  @ResponseStatus(HttpStatus.OK)
  public List<Inventory> findBySkuCodes(@RequestBody @Valid SkuCodeDTO skuCodeDTO) {
    return inventoryService.findBySkuCodeIn(skuCodeDTO.getSkuCodes());
  }
}
