package com.lth.inventory_service.service.implement;

import com.lth.inventory_service.service.InventoryService;
import com.lth.inventory_service.dto.InventoryDTO;
import com.lth.inventory_service.entity.Inventory;
import com.lth.inventory_service.repository.InventoryRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class InventoryServiceImpl implements InventoryService {

  private final InventoryRepository inventoryRepository;

  @Override
  public Inventory create(InventoryDTO inventoryDTO) {
    Inventory inventory = Inventory.of(
        inventoryDTO.getSkuCode(),
        inventoryDTO.getQuantity()
    );
    log.info("(create)inventory: {}", inventory);

    return inventoryRepository.save(inventory);
  }

  @Override
  public List<Inventory> findAll() {
    return inventoryRepository.findAll();
  }

  @Override
  public Inventory findBySkuCode(String skuCode) {
    return inventoryRepository.findBySkuCode(skuCode);
  }

  @Override
  public List<Inventory> findBySkuCodeIn(List<String> skuCodes) {
    return inventoryRepository.findBySkuCodeIn(skuCodes);
  }
}
