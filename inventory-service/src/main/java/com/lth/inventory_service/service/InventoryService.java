package com.lth.inventory_service.service;

import com.lth.inventory_service.dto.InventoryDTO;
import com.lth.inventory_service.entity.Inventory;
import java.util.List;

public interface InventoryService {

  Inventory create(InventoryDTO inventoryDTO);

  List<Inventory> findAll();

  Inventory findBySkuCode(String skuCode);

  List<Inventory> findBySkuCodeIn(List<String> skuCodes);
}
