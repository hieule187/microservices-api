package com.lth.inventory_service.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Data;

@Entity
@Data
public class Inventory {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column(unique = true)
  private String skuCode;
  private Integer quantity;

  public static Inventory of(String skuCode, Integer quantity) {
    Inventory inventory = new Inventory();
    inventory.setSkuCode(skuCode);
    inventory.setQuantity(quantity);

    return inventory;
  }
}
