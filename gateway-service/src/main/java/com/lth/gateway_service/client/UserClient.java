package com.lth.gateway_service.client;

import com.lth.gateway_service.dto.response.SuccessResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class UserClient {

  private final WebClient webClient;

  public UserClient(WebClient.Builder webClientBuilder) {
    this.webClient = webClientBuilder.baseUrl("http://user-service").build();
  }

  public Mono<SuccessResponse> verifyToken(String tokenId) {
    return webClient.get()
        .uri("/api/v1/auth/verify-token/{tokenId}", tokenId)
        .retrieve()
        .bodyToMono(SuccessResponse.class);
  }

}
