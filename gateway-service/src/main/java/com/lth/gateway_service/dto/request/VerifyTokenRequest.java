package com.lth.gateway_service.dto.request;

import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class VerifyTokenRequest {

  @NotBlank
  private String accessToken;

}
