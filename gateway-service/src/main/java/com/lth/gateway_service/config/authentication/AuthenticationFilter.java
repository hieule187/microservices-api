package com.lth.gateway_service.config.authentication;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lth.gateway_service.client.UserClient;
import com.lth.gateway_service.constant.MessageSourceConstant;
import com.lth.gateway_service.dto.response.ErrorResponse;
import java.util.Arrays;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Component
@Slf4j
@RequiredArgsConstructor
public class AuthenticationFilter implements GlobalFilter, Ordered {

  private static final String[] PUBLIC_ENDPOINTS = {
      "/api/v1/auth/.*",
      "/api/v1/.*/public(/.*)?"
  };

  private final UserClient userClient;
  private final ObjectMapper objectMapper;

  @Override
  public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
    if (isPublicEndpoint(exchange.getRequest())) {
      return chain.filter(exchange);
    }

    List<String> authHeaders = exchange.getRequest().getHeaders().get(HttpHeaders.AUTHORIZATION);

    if (CollectionUtils.isEmpty(authHeaders)) {
      return unauthenticated(exchange.getResponse());
    }

    String tokenId = authHeaders.get(0).replace("Bearer ", "");
    log.info("(authenticationFilter)tokenId: {}", tokenId);

    return userClient.verifyToken(tokenId).flatMap(data -> {
      log.info("(authenticationFilter)verifyToken: {}", data);

      if (data != null && Boolean.TRUE.equals(data.getData())) {
        return chain.filter(exchange);
      } else {
        return unauthenticated(exchange.getResponse());
      }
    }).onErrorResume(exception -> unauthenticated(exchange.getResponse()));
  }

  @Override
  public int getOrder() {
    return -1;
  }

  private boolean isPublicEndpoint(ServerHttpRequest request) {
    return Arrays.stream(PUBLIC_ENDPOINTS)
        .anyMatch(endpoint -> request.getURI().getPath().matches(endpoint));
  }

  private Mono<Void> unauthenticated(ServerHttpResponse response) {
    ErrorResponse errorResponse = ErrorResponse.of(
        HttpStatus.UNAUTHORIZED.value(),
        MessageSourceConstant.UNAUTHORIZED_EXCEPTION,
        "Unauthorized."
    );

    String body;
    try {
      body = objectMapper.writeValueAsString(errorResponse);
    } catch (JsonProcessingException exception) {
      throw new IllegalArgumentException(exception);
    }

    response.setStatusCode(HttpStatus.UNAUTHORIZED);
    response.getHeaders().add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

    return response.writeWith(Mono.just(response.bufferFactory().wrap(body.getBytes())));
  }
}
